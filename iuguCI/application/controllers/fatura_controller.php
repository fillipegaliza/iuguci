<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require 'application//third_party/iugu/lib/Iugu.php';
class Fatura_controller extends CI_Controller {
	public function index() {
	}
	public function create_fatura() {
		
		
		
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]|strtolower|valid_email');
		$this->form_validation->set_rules('cc_emails', 'CC', 'trim|required|max_length[50]|strtolower|valid_email');
		$this->form_validation->set_rules('description', 'Login', 'trim|max_length[130]|strtolower');
		$this->form_validation->set_rules('quantity', 'Quantidade', 'trim|numeric|strtolower');
		$this->form_validation->set_rules('value', 'Valor', 'trim|numeric|strtolower');
		
// 		if($this->form_validation->run()==TRUE)
// 		{
// 			$dados = elements(array('email', 'cc_emails', 'description', 'quantity', 'value'), $this->input->post());
// 			$this->crud_model->do_insert($dados);
		
		
		
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		if(isset($_POST['draft'])){
			$status = "draft";
		}
		else{
			$status = "pending";
		}
		
		if(isset($_POST['ignore_due_email'])){
			$ignore = TRUE;
		}
		else{
			$ignore = FALSE;
		}
		
		
		
		$return = Iugu_Invoice::create ( Array (
				"email" => $_POST['email'],
				"status" => $status,
				"cc_emails" => $_POST['cc_emails'],
				"due_date" => $_POST['due_date'],
				"client_id" => "11B74C989B3047B1817DE9D07FCEAB70",
				"items" => Array (
						Array (
								"description" => $_POST['description'],
								"quantity" => $_POST['quantity'],
								"price_cents" => $_POST['price_cents']
						) 
				),
				"custom_variables" => Array(
							Array(
									"name" => $_POST['name'],
									"value" => $_POST['value']
						)
				),
				"payable_with" => $_POST['payable_with'],
				"ignore_due_email" => $ignore, 
		) );
		print_r ( $return );
	}
	
	
	
	public function retrieve_fatura() { //OK
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$invoice = Iugu_Invoice::fetch("28EB5896AD814A629B9A9EBAD90B1B1E");
		
		print_r($invoice);
				
		}
	
	
	public function cancel_fatura() { //OK
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$invoice = Iugu_Invoice::fetch("C69934A223004E0CBD25811EE23947FD");		
		$invoice->cancel();
	
		//print_r ( $invoice );
	}

	
	public function refund_fatura() { //OK
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$invoice = Iugu_Invoice::fetch("C69934A223004E0CBD25811EE23947FD");		
		$invoice->refund();
	
		//print_r ( $invoice );
	}
	public function listar_fatura() { //OK
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$invoices = Iugu_Invoice::search()->results();
	
		print_r($invoices);
	}
}

	
	
	

