<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require 'application//third_party/iugu/lib/Iugu.php';
class Subconta_controller extends CI_Controller {
	public function index() {
	}
	public function create_subconta() {
		Iugu::setApiKey ( '238483b95e0252c2d621bcb61bc6d7f9' );
		$requestIugu = new Iugu_APIRequest ();
		
		$url = 'https://api.iugu.com/v1/marketplace/create_account';
		
		$dados = array (
				"name" => 'SubcontaTeste',
				"commission_percent" => 10 
		);
		
		$teste = $requestIugu->request ( 'post', $url, $dados );
		
		var_dump ( $teste );
	}
	public function saque_subconta() {
		Iugu::setApiKey ( '238483b95e0252c2d621bcb61bc6d7f9' );
		
		$id_subconta = "fdb09cbd974da91d9e109617ab69ce0a";
		
		$requestIugu = new Iugu_APIRequest ();
		
		$url = 'https://api.iugu.com/v1/accounts/' . $id_subconta . '/request_withdraw';
		
		$dados = array (
				"amount" => 10 
		);
		
		$teste = $requestIugu->request ( 'post', $url, $dados );
		
		var_dump ( $teste );
	}
	public function config_subconta() {
		Iugu::setApiKey ( 'fdb09cbd974da91d9e109617ab69ce0a' );
		
		$requestIugu = new Iugu_APIRequest ();
		
		$url = 'https://api.iugu.com/v1/accounts/configuration';
		
		$dados = array (
				"commission_percent" => 1,
				"auto_withdraw" => FALSE,
				"fines" => TRUE,
				"per_day_interest" => TRUE,
				"late_payment_fine" => 2,
				"bank_slip" => Array (
						
						"active" => TRUE,
						"extra_due" => TRUE,
						"reprint_extra_due" => 1 
				),
				"credit_card" => Array (
						
						"avtive" => TRUE,
						"soft_descriptor" => "teste",
						"installments" => TRUE,
						"installments_pass_interest" => TRUE,
						"max_installments" => "12",
						"max_installments_without_interest" => "2",
						"two_step_transaction" => TRUE 
				) 
		);
		
		$teste = $requestIugu->request ( 'post', $url, $dados );
		
		var_dump ( $teste );
	}
}