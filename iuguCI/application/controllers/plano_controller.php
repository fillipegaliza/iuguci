<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require 'application//third_party/iugu/lib/Iugu.php';
class Plano_controller extends CI_Controller {
	public function index() {
	}
	public function create_plano() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$return = Iugu_Plan::create ( Array (
				"name" => "Plano Teste Magico",
				"identifier" => "greatest_plan", //N�o pode se repetir
				"interval" => "2",
				"interval_type" => "weeks",
				"payable_with" => "all",
				"currency" => "BRL",
				"value_cents" => "666",
				
				"features" => Array (
						Array (
								"name" => "Funcionalidade Teste",
								"identifier" => "teste", //N�o pode se repetir
								"value" => "20000" 
						) 
				)
		) );
		print_r ( $return );
	}



	public function retrieve_plano() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$plano = Iugu_Plan::fetch ( "57A1290F310740B3B569D3EB7659063A" );
		
		print_r ( $plano );
	}



	public function update_plano	() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$plano = Iugu_Plan::fetch( "57A1290F310740B3B569D3EB7659063A" );
		$plano->name = "Evil MASTER Plan";
		$plano->interval = "1";
		$plano->interval_type = "months";
		$plano->payable_with = "all";
		$plano->currency = "BRL";
		$plano->features =  Array (
						Array (
								"name" => "Funcionalidade TESTE 2",
								"identifier" => "testetesteteste", //N�o pode se repetir
								"value" => "7000" 
						) 
				);	
		$plano->prices =  Array (
						Array (
								"currency" => "BRL",
								"value_cents" => "7000" 
						) 
				);	
		$plano->save();
		print_r($plano);
	}


	
	public function listar_plano()
	{
		Iugu::setApiKey("238483b95e0252c2d621bcb61bc6d7f9");
		
		$plano = Iugu_Plan::search()->results();
		
		print_r($plano);
	}



	
	
	public function delete_plano() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$plano = Iugu_Plan::fetch ( "5E9AB8DB067441AEB63269476B5933E9" );
		$plano->delete ();
		
		print_r ( $plano );
	}
}
	

