<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require 'application//third_party/iugu/lib/Iugu.php';
class Cliente_controller extends CI_Controller {
	public function index() {
	}
	public function create_cliente() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$return = Iugu_Customer::create ( Array (
				"email" => "fillipe@teste.com",
				"cc_emails" => "teste@teste.com",
				"cpf_cnpj" => "66666666666",
				"name" => "Fillipe Galiza",
				"notes" => "TESTE",
				
				"custom_variables" => Array (
						Array (
								"name" => "Variavel teste CLIENTE",
								"value" => "999666" 
						) 
				) 
		) );
		print_r ( $return );
	}
	public function retrieve_cliente() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$customer = Iugu_Customer::fetch ( "87C8028936F24BEC9E915703D5B92678" );
		
		print_r ( $customer );
	}
	public function delete_cliente() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$cliente = Iugu_Customer::fetch ( "87C8028936F24BEC9E915703D5B92678" );
		$cliente->delete ();
		
		print_r ( $cliente );
	}
	public function update_cliente() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$customer = Iugu_Customer::fetch ( "82041CC4C1B2461C8DC2AB36FC5D9E14" );
		$customer->name = "Maria da Silva";
		$customer->email = "maria@maria.com";
		$customer->cc_emails = "teste@teste.com";
		$customer->cpf_cnpj = "teste@teste.com";
		$customer->notes = "Muito simpática";
		$customer->custom_variables = Array (
				Array (
						"name" => "Variavel da Maria",
						"value" => "1000" 
				) 
		);
		
		$customer->save ();
	}
	
	public function listar_cliente()
	{
		Iugu::setApiKey("238483b95e0252c2d621bcb61bc6d7f9");
		
		$customer = Iugu_Customer::search()->results();
		
		print_r($customer);
	}
	
	
}
	

