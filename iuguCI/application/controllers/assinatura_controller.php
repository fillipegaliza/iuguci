<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require 'application//third_party/iugu/lib/Iugu.php';
class Assinatura_controller extends CI_Controller {
	public function index() {
	}
	public function create_assinatura() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$return = Iugu_Subscription::create ( Array (
				"plan_identifier" => "great_plan",
				"expires_at" => "2016-11-19T11:24:29-02:00",
				"suspended" => "false",
				"customer_id" => "86792335334C46EFB0E89A2C1DB2FBF8",
				"payable_with" => "all",
				"subitems" => Array (
						Array (
								"description" => "Item Um",
								"quantity" => "1",
								"price_cents" => "1000" 
						) 
				),
				"custom_variables" => Array (
						Array (
								"name" => "Variavel Um",
								"value" => "10" 
						) 
				) 
		) );
		print_r ( $return );
	}
	public function retrieve_assinatura() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$assinatura = Iugu_Subscription::fetch ( "8638AE285FC3415DA782AA089E651443" );
		
		print_r ( $assinatura );
	}




	public function update_assinatura	() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$assinatura = Iugu_Subscription::fetch( "8638AE285FC3415DA782AA089E651443" );
		$assinatura->plan_identifier = "great_plan";
		$assinatura->expires_at = "2018-11-19T11:24:29-02:00";
		$assinatura->payable_with = "all";
		$assinatura->suspended = false;
		$assinatura->subitems = Array (
						Array (
								"description" => "Item dois",
								"quantity" => "1",
								"price_cents" => "1000" 
						) 
				);
		$assinatura->custom_variables = Array (
						Array (
								"name" => "Variavel dois",
								"value" => "10"
						) 
				);
		$assinatura->save();
		print_r($assinatura);
	}


	
	public function listar_assinatura()
	{
		Iugu::setApiKey("238483b95e0252c2d621bcb61bc6d7f9");
		
		$assinatura = Iugu_Subscription::search()->results();
		
		print_r($assinatura);
	}



	
	
	public function delete_assinatura() {
		Iugu::setApiKey ( "238483b95e0252c2d621bcb61bc6d7f9" );
		
		$plano = Iugu_Subscription::fetch ( "8638AE285FC3415DA782AA089E651443" );
		$plano->delete ();
		
		print_r ( $plano );
	}
}
	


	
