<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Text Date Field
 *
 * @access public
 * @param mixed
 * @param string
 * @param string
 * @return string
 */
if ( ! function_exists('form_date'))
{
function form_date($data = '', $value = '', $extra = '')
{
$defaults = array('type' => 'date', 'class' => 'datepicker');
return "<input "._parse_form_attributes($data, $defaults).$extra." />";
}
}