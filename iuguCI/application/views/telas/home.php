
<div class="col l12 center-align" id="central-top">
	<ul id="lista-topo">
		<li>
			<div>Ultima Transferencia</div>
		</li>
		<li>
			<div>Total de Assinantes</div>
		</li>
		<li>
			<div>Volume este mes</div>
		</li>
		<li>
			<div>Volume mes anterior</div>
		</li>
	</ul>
</div>
<div class="col l12" id="central-middle-home">
	<div id="content-home center-align">
		<h5>Méricas e Performance</h5>
		<p>Com base nos dados dos últimos 30 dias</p>
	</div>
	<div>
		<ul id="content-home-graph">
			<li>
				<div>Transações</div>
			</li>
			<li>
				<div>Volume</div>
			</li>
			<li>
				<div>Assinaturas</div>
			</li>
		</ul>

	</div>

	<div id="">
		<h5>Saldo e informações da conta</h5>
		<table id="table-home" class="hoverable bordered responsive-table">
			<thead>
				<tr>
					<th data-field="saldo">Saldo</th>
					<th data-field="transito">Em Transito</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>R$ 0,00</td>
					<td>$0.87</td>
				</tr>
			</tbody>
		</table>



		<table id="table-home" class="hoverable bordered responsive-table">
			<thead>
				<tr>
					<th data-field="saldo">A Receber</th>
					<th data-field="saldo">Comissões</th>
					<th data-field="transito">Tarifas</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>R$ 0,00</td>
					<td>R$ 0,00</td>
					<td>$0.87</td>
				</tr>
			</tbody>
		</table>
		
		<a id ="home-btn" class="waves-effect waves-light btn">SOLICITAR TRANSFERÊNCIA</a>
		
		<h5>Taxas Pagas</h5>
		<table id="table-home" class="hoverable bordered responsive-table center-align">
			<thead>
				<tr>
					<th data-field="saldo">Este Mês</th>
					<th data-field="transito">Mês Anterior</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>R$ 0,00</td>
					<td>$0.87</td>
				</tr>
			</tbody>
		</table>

	</div>

	<div></div>
</div>
<div class="row">
	<div class="col s2"></div>
	<div class="col s2"></div>
</div>