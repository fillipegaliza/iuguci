
<div class="row">
	<div class="col s2"></div>
	<div class="col s2" id="lateral">
		<div>
		
			LOGO
					
		</div>
	
		<ul class="collapsible" data-collapsible="accordion">
			<li>
				<div class="collapsible-header">
					<a href="">HOME</a>
				</div>
			</li>
							
			<li>
				<div class="collapsible-header">

					<a href="">Faturas</a>
				</div>
				<div class="collapsible-body">

					<ul>
						<li><a href="">Expirada<span class="badge"></span></a></li>
						<li><a href="">Pendente<span class="badge"></span></a></li>
						<li><a href="">Vencida<span class="badge"></span></a></li>
						<li><a href="">Rascunho<span class="badge"></span></a></li>
						<li><a href="">Paga<span class="badge"></span></a></li>
						<li><a href="">Cancelada<span class="badge"></span></a></li>
						<li><a href=""></a></li>
					</ul>
				</div>
			</li>
			<li>
				<div class="collapsible-header">
					<a href="">Assinaturas</a>
				</div>
				<div class="collapsible-body">
					<ul>
						<li><a href="">Vencida<span class="badge"></span></a></li>
						<li><a href="">Ativa<span class="badge"></span></a></li>
						<li><a href="">Inativa<span class="badge"></span></a></li>
					</ul>
				</div>
			</li>
			<li>
				<div class="collapsible-header">
					<a href="">Clientes</a>
				</div>
			</li>
			<li>
				<div class="collapsible-header">
					<a href="">Relatórios</a>
				</div>
				<div class="collapsible-body">
					<ul>
						<li><a href="">Crescimento<span class="badge"></span></a></li>
						<li><a href="">Cancelamento<span class="badge"></span></a></li>
						<li><a href="">Retenção<span class="badge"></span></a></li>
						<li><a href="">Transferência<span class="badge"></span></a></li>
						<li><a href="">Extrato Financeiro<span class="badge"></span></a></li>
						<li><a href="">Extrato de Faturas<span class="badge"></span></a></li>
					</ul>
				</div>
			</li>
			<li>
				<div class="collapsible-header">
					<a href="">Administração</i></a>
				</div>
				<div class="collapsible-body">
					<ul>
						<li><a href="">Configurações Gerais<span class="badge"></span></a></li>
						<li><a href="">Seus Bancos e Instituições<span class="badge"></span></a></li>
						<li><a href="">Métodos de Pagamento<span class="badge"></span></a></li>
						<li><a href="">Gestão de Cobrança<span class="badge"></span></a></li>
						<li><a href="">Envio de Emails<span class="badge"></span></a></li>
						<li><a href="">Planos<span class="badge"></span></a></li>
						<li><a href="">Gatilhos<span class="badge"></span></a></li>
						<li><a href="">Configurações da Conta<span class="badge"></span></a></li>
					</ul>
				</div>
			</li>
			<li>
			<div class="collapsible-header">
				<a href="">Sair</a>
			</div>
			</li>		
		</ul>

	</div>